#include "Cannon.h"


Cannon::Cannon(float iX, float iY)
{
	//initial power and angle values
	Iangle = 0;
	oAngle=0;
	power = 0;
	//maximum and minimum angles which the gun can rotate through
	maxAngle = 90;
	minAngle = 0;
	//position of the gun fulcrum point
	x = iX;
	y = iY;
	//vertices for the gun barrel
	barrel[0] = new Vertex(x,y);
	barrel[1] = new Vertex(x,y+16);
	barrel[2] = new Vertex(x+64,y+16);
	barrel[3] = new Vertex(x+64,y);
	//set up new bitmaps to represent the barrel and the wheel
	bitmap = new Bitmap("barrel.bmp", true);
	wheel = new Bitmap("wheel.bmp",true);
}
float Cannon::getAngle()
{
	//returns angle of cannon barrel for purposes of bullet generation
	oAngle = Iangle*3.1415927/180;
	return oAngle;

}
Cannon::~Cannon(void)
{
}
Vertex Cannon::getBulletOrigin()
{
	//returns a vertex based on internally stored angle, converted to radians
	oAngle = Iangle*3.1415927/180;
	return Vertex(barrel[0]->X()+(cos(oAngle)*64),barrel[0]->Y()+8+(sin(oAngle)*64));
}
void Cannon::cannonRotate(float step)
{
	//if barrel angle is between 0 and 90 degrees, increment angle by input value.
	if (Iangle <= maxAngle && Iangle >= minAngle)
	{
		Iangle += step;
	}
	if (Iangle <= minAngle)
	{
		Iangle = minAngle;
	}
	if (Iangle >=maxAngle)
	{
		Iangle = maxAngle;
	}
}
void Cannon::reset()
{
	Iangle = 0;
}
void Cannon:: drawCannon()
{	
	//rotate barrel texture by Iangle then draw it		
	glPushMatrix();
	glTranslatef(barrel[0]->X(),barrel[0]->Y(),0);
	glRotatef(Iangle, 0,0,1);
	glTranslatef(-(barrel[0]->X()),-(barrel[0]->Y()),0);
	bitmap->drawAt(barrel[0]->X(), barrel[0]->Y());
	glPopMatrix();
	//then draw the wheel over the barrel
	wheel->drawAt(x-16,y-16);
	

}
