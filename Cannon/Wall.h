#pragma once
#include"GL/glut.h"
#include "Vertex.h"
class Wall
{
public:
	Wall(float iX, float iY, float iWidth, float iHeight);
	float getLocationX();
	float getLocationY();
	float getHeight();
	float getWidth();
	void draw();
	~Wall(void);
private:
	float x;
	float y;
	float width;
	float height;
	Vertex *points[4];
};

