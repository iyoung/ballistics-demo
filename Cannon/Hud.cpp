#include "Hud.h"


Hud::Hud(float iX, float iY)
{
	x=iX;
	y=iY;
	shot = new Bitmap("shots1.bmp", true);
	mainhud = new Bitmap("mainhud.bmp",true);
	bullets = 3;
}

void Hud::setShots(int mod)
{
	bullets+= mod;
}
int Hud::getShots()
{
	return bullets;
}
void Hud::draw()
{
	mainhud->drawAt(x,y);
	for (int i =0;i<bullets;++i)
	{
		shot->drawAt(x+8+(i*16),y+4);
	}
}
void Hud::reset()
{
	bullets = 3;
}
Hud::~Hud(void)
{
}
