#pragma once
#include "Vertex.h"
#include"GL/glut.h"
#include<cmath>
#include"Bitmap.h"
#include"Explosion.h"
class Cannon
{
public:
	Cannon(float iX, float iY);
	~Cannon(void);
	void cannonRotate(float step);
	float getAngle();
	void drawCannon();
	void reset();
	Vertex getBulletOrigin();
private:
	float x;
	float y;
	float Iangle;
	float oAngle;
	float power;
	bool increasing;
	float maxAngle;
	float minAngle;
	float oX;
	float oY;
	Vertex *barrel[4];
	Bitmap *bitmap;
	Bitmap *wheel;
};

