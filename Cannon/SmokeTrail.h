#pragma once
#include "GL/glut.h"
#include <cmath>
class SmokeTrail
{
public:
	SmokeTrail(float iX, float iY);
	void draw();
	bool getShow();
	void setShow(bool indicator);
	~SmokeTrail(void);
private:
	float x;
	float y;
	float radius;
	float opacity;
	bool show;
};

