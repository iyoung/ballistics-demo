#pragma once
#include "Vertex.h"
#include <cmath>
#include"GL/glut.h"
#include"Bitmap.h"
class Bullet
{
public:
	Bullet(Vertex origin,float iHeight, float iWidth, float power, float iAngle);
	void move();
	void drawBullet();
	void doAcceleration();
	float getXVel();
	float getYVel();
	float getLocationX();
	float getLocationY();
	void setShow(bool indicator);
	bool getShow();
	~Bullet(void);
private:
	float x;
	float y;
	float dX;
	float dY;
	//float angle;
	float gravity;
	bool show;
	Vertex *points[3];
	float cx, cy;
	Bitmap *bullet;
};

