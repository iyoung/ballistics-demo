#include "Explosion.h"


Explosion::Explosion(float iX, float iY)
{
	x = iX;
	y = iY;
	radius = 0;
	step = 0.04;
	opacity = 1;
	show = true;
}
Explosion::Explosion(Vertex inV)
{
	x=inV.X();
	y=inV.Y();
	radius = 0;
	step = 0.04;
	opacity = 1;
	show = true;
}
void Explosion::setShow(bool indicator)
{
	show = indicator;
}
void Explosion::draw()
{
	//if show is true, draw two circles, which will expand each time the function is called.
	if(show)
		{
			radius+=step*25;
			opacity-=step;
			const int NPOINTS=25;
			const float TWOPI=2*3.1415927;
			const float STEP=TWOPI/NPOINTS;

			glBegin(GL_POLYGON);
				for (float angle=0;angle<TWOPI;angle+=STEP)
				{
					glColor4f(1,0,0,opacity);
					glVertex2f(x+radius*cos(angle), y+radius*sin(angle));
				}
			glEnd();

			glBegin(GL_POLYGON);
				for (float angle=0;angle<TWOPI;angle+=STEP)
				{
					glColor4f(1,1,0,opacity);
					glVertex2f(x+(radius/2)*cos(angle), y+(radius/2)*sin(angle));
				}
			glEnd();
		}
	if (radius > 30)
	{
		show = false;
	}
}
Explosion::~Explosion(void)
{
}
