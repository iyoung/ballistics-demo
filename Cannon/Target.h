#pragma once
#include"GL/glut.h"
#include <cmath>
class Target
{
public:
	Target(float iX, float iY, float iRadius);
	float getX();
	float getY();
	float getRadius();
	void draw();
	void setShow(bool indicator);
	bool getShow();
	~Target(void);
private:
	float radius;
	float x;
	float y;
	bool show;
};

