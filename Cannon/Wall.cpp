#include "Wall.h"


Wall::Wall(float iX, float iY, float iWidth, float iHeight)
{
	x = iX;
	y = iY;
	width = iWidth;
	height = iHeight;
	points[0] = new Vertex(x,y);
	points[1] = new Vertex(x,y+iHeight);
	points[2] = new Vertex(x+iWidth,y+iHeight);
	points[3] = new Vertex(x+iWidth,y);

}
float Wall::getLocationX()
{
	float fulcrumX = 0;
	for (int i = 0; i < 4;i++)
	{
		fulcrumX+=points[i]->X();
	}
	return fulcrumX/=4;
}
float Wall::getLocationY()
{
	float fulcrumY = 0;
	for (int i = 0; i < 4;i++)
	{
		fulcrumY+=points[i]->Y();
	}
	return fulcrumY/=4;
}
float Wall::getHeight()
{
	return height;
}
float Wall::getWidth()
{
	return width;
}
void Wall::draw()
{
	glBegin(GL_POLYGON);
	glColor3f(1,1,0);
	for(int i = 0;i < 4; i++)
	{
		glVertex2f(points[i]->X(),points[i]->Y());
	}
	glEnd();
}
Wall::~Wall(void)
{
}
