#include "SmokeTrail.h"


SmokeTrail::SmokeTrail(float iX, float iY)
{
	x = iX;
	y = iY;
	radius = 0;
	opacity = 1;
	show = true;
}
void SmokeTrail::setShow(bool indicator)
{
	show = indicator;
}
bool SmokeTrail::getShow()
{
	return show;
}
void SmokeTrail::draw()
{
	const int NPOINTS=25;
	const float TWOPI=2*3.1415927;
	const float STEP=TWOPI/NPOINTS;
	const float step = 0.1;
	if (show)
	{
		radius+= (step*10);
		glBegin(GL_POLYGON);
		for (float angle=0;angle<TWOPI;angle+=STEP)
				{
					glColor4f(1,1,1,opacity);
					glVertex2f(x+radius*cos(angle), y+radius*sin(angle));
				}
		glEnd();

		opacity-= step;
	if(opacity <= 0)
	{
		show = false;
	}
	}
}
SmokeTrail::~SmokeTrail(void)
{
}
