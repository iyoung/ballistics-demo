Bitmap class & Simple Test Project
==================================
The files Bitmap.h and Bitmap.cpp define a class for using bitmap (*.bmp) files in an OpenGL project.
Include both files in an OpenGL project and create a Bitmap object as shown in main.cpp.
In Visual Studio, the files should be organised with Bitmap.h in the Header Files folder of the project, 
Bitmap.cpp in the Source Files folder and any bitmap files you wish to use in the Resource Files folder.

Necessary steps for loading and displaying a bitmap:

1) Declare a Bitmap pointer variable, e.g. 

	Bitmap *myBitmap = NULL;

2) In an initialization routine (or near the top of main()), create
   the bitmap, passing the file name as the first parameter, and a
   boolean indicating whether the bitmap is to handle transparency
   as the second.  See the notes on transparency at the end of this.
   For example:

	myBitmap = new Bitmap("MyFile.bmp", false);	// No transparency.

   or:

	myBitmap = new Bitmap("MyFile.bmp", false);	// With transparency.


3) In your gl display function, call the draw() method, passing the top-left
   and bottom-right coordinates of the area the bitmap is to be drawn into.
   Whatever its size, the bitmap will be scaled to fit this area.  For example:

	myBitmap->draw(10, 10, 50, 50);	// Will draw a 40*40 bitmap at 10, 10

   Note that the coordinates used will depend on the projection used.  For 3D, 
   this will depend on camera position etc. (frustrum).  In 2D, you should be
   using a glOrtho2D() setting that sets the screen up as an array of pixels,
   e.g.:

	glOrtho2D(0, windowWidth, 0, windowHeight);  // For 0, 0 at bottom left.

    or:

	glOrtho2D(0, windowWidth, windowHeight, 0);	// For 0, 0 at top-left.


4) Remember to delete the bitmap when you are done with it as it will occupy a
   significant amount of memory.  In many cases, this will be done on exit (in
   which case it is not strictly necessary, but you ought to do it anyway.  e.g.:

	delete myBitmap;


5) The Bitmap class supports a wide range of bitmap formats.  However, a constraint 
   is that the width and height of the file should be a multiple of 4 pixels, so a 
   bitmap of 16x16 will work, but one of 16*15 might not for some formats.  Best bet 
   is to use the truecolour (24-bit bitmap) format, since this does not have the 
   multiple of 4 constraint.

(c) A McMonnies, 2011.


Transparency
============
Note, for transparency the top-left pixel of the bitmap is the one that will be the chroma-key colour.  Every other pixel that is *exactly* this colour will be treated as transparent.  To use transparency, the display will need to be set-up to support it, which means calling glutInitDisplayMode() as:

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);


or:

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
