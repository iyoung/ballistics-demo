#pragma once
#include <string>
#include"GL/glut.h"
#include"Bitmap.h"

using namespace std;

class Hud
{
public:
	Hud(float iX, float iY);
	int getShots();
	void setShots(int mod);
	void draw();
	void reset();
	~Hud(void);
private:
	float x;
	float y;
	int bullets;
	Bitmap *shot;
	Bitmap *mainhud;
};

