#pragma once
#include"GL/glut.h"
#include <cmath>
#include"Vertex.h"
class Explosion
{
public:
	Explosion(float iX, float iY);
	Explosion(Vertex inV);
	void draw();
	void setShow(bool indicator);
	~Explosion(void);
private:
	float x;
	float y;
	float radius;
	float step;
	float opacity;
	bool show;
};

