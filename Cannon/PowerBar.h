#pragma once
#include "Vertex.h"
#include"GL/glut.h"
class PowerBar
{
public:
	PowerBar(float iX, float iY);
	void resize(float resizeAmount);
	void reset();
	void drawPowerBar();
	bool reachMax();
	bool reachMin();
	float getPower();
	~PowerBar(void);

private:
	float x;
	float y;
	float width;
	float height;
	bool increasing;
	Vertex *outer[4];
	Vertex *inner[4];
};

