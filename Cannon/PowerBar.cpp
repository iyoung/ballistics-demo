#include "PowerBar.h"


PowerBar::PowerBar(float iX, float iY)
{
	x=iX;
	y=iY;
	height = 10;
	width = 100;
	increasing = true;
	//define outer bar vertex co-ords
	outer[0] = new Vertex(x,y);
	outer[1] = new Vertex(x,y+height);
	outer[2] = new Vertex(x+width,y+height);
	outer[3] = new Vertex(x+width,y);
	//define inner bar vertex co-ords
	inner[0] = new Vertex(x,y);
	inner[1] = new Vertex(x,y+height);
	inner[2] = new Vertex(x,y+height);
	inner[3] = new Vertex(x,y);
}
void PowerBar::reset()
{
	//sets power level back to zero
	inner[2]->setX(x);
	inner[3]->setX(x);
}
void PowerBar::resize(float resizeAmount)
{
	//inner power bar will resize between 0 and 100 width
	
	if(reachMin())
	{
		increasing = true;
	}
    if (reachMax())
	{
		increasing = false;
	}
	if (increasing)
	{
		inner[2]->addX(resizeAmount);
		inner[3]->addX(resizeAmount);
	}
	if(!increasing)
	{
		inner[2]->subX(resizeAmount);
		inner[3]->subX(resizeAmount);
	}
	
}
void PowerBar::drawPowerBar()
{
	//draw inner bar first
	glBegin(GL_POLYGON);
	glColor3f(1,0,0);
	for (int i = 0; i<4; i++)
	{
		glVertex2f(inner[i]->X(),inner[i]->Y());
	}
	glEnd();
	//then draw outer border over it
	glBegin(GL_LINE_LOOP);
	glColor3f(0,0,1);
	for (int i = 0; i<4; i++)
	{
		glVertex2f(outer[i]->X(),outer[i]->Y());
	}
	glEnd();
}
bool PowerBar::reachMax()
  { 
	//used to determine if power has reached maximum possible value
	if (inner[2]->X() >= outer[2]->X())
	{
		return true;
	}
	else
		return false;
}
bool PowerBar::reachMin()
{
	//used to determine if power has reached minimum possible value
	if (inner[2]->X() <= outer[1]->X())
	{
		return true;
	}
	else
	{
		return false;
	}
}
float PowerBar::getPower()
{
	//returns current power level.
	return inner[2]->X()-inner[1]->X();
}
PowerBar::~PowerBar(void)
{
}
