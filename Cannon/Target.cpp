#include "Target.h"


Target::Target(float iX, float iY, float iRadius)
{
	x = iX;
	y = iY;
	radius = iRadius;
	show = true;
}
float Target::getRadius()
{
	//returns the radius of the target
	return radius;
}
float Target::getX()
{
	//returns the current x co-ordinate of this object
	return x;
}
float Target::getY()
{
	//returns the current y co-ordinate of this object
	return y;
}
void Target::draw()
{
	//if show is true, draw three circles to represent the target on screen
	if (show)
	{
		const int NPOINTS=25;
		const float TWOPI=2*3.1415927;
		const float STEP=TWOPI/NPOINTS;
	glBegin(GL_POLYGON);
		for (float angle=0;angle<TWOPI;angle+=STEP)
		{
			glColor3f(1,0,0);
			glVertex2f(x+radius*cos(angle), y+radius*sin(angle));
		}
	glEnd();
	
	glBegin(GL_POLYGON);
		for (float angle=0;angle<TWOPI;angle+=STEP)
		{
			glColor3f(1,1,1);
			glVertex2f(x+(radius*0.66)*cos(angle), y+(radius*0.66)*sin(angle));
		}
	glEnd();

	glBegin(GL_POLYGON);
		for (float angle=0;angle<TWOPI;angle+=STEP)
		{
			glColor3f(0,0,1);
			glVertex2f(x+(radius/3)*cos(angle), y+(radius/3)*sin(angle));
		}
	glEnd();
	}
}
void Target::setShow(bool indicator)
{
	//this is used to set wether the target should be drawn
	show = indicator;
}
bool Target::getShow()
{
	//this is a status accessor
	return show;
}
Target::~Target(void)
{
}
