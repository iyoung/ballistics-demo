#include<iostream>
#include "Bitmap.h"
#include"GL/glut.h"
#include <cmath>
#include "Cannon.h"
#include "PowerBar.h"
#include "Bullet.h"
#include "Vertex.h"
#include "Wall.h"
#include "Target.h"
#include "Explosion.h"
#include "Hud.h"
#include "SmokeTrail.h"

using namespace std;
//global variables and constants
const int MAXWALLS = 3;
const int MAXSMOKE = 5;
Cannon *gameCannon;
PowerBar *powerbar;
Bullet  *bullet;
Target *target;
Explosion *explosion;
Wall *walls[MAXWALLS];
Bitmap *background;
Hud *hud;
Bitmap *controls;
Bitmap *gameover;
bool *newGame;
SmokeTrail *smoketrails[MAXSMOKE];
int numsmoketrails = 0;

void init()
{
	//generate new game objects
	gameCannon = new Cannon(23,36);
	powerbar = new PowerBar(5,5);
	walls[0] = new Wall(768,20,32,580);
	walls[1] = new Wall(538,20,32,100);
	walls[2] = new Wall(0,0,800,20);
	target = new Target(748,40,20);
	bullet = new Bullet(gameCannon->getBulletOrigin(),15,20,powerbar->getPower(),gameCannon->getAngle());
	bullet->setShow(false);
	background = new Bitmap("background.bmp", true);
	controls = new Bitmap("controls.bmp",true);
	gameover = new Bitmap("gameover.bmp",true);
	hud = new Hud(20,550);
	newGame = new bool(true);
	for (int i = 0; i< MAXSMOKE; ++i)
	{
		smoketrails[i] = new SmokeTrail(bullet->getLocationX(),bullet->getLocationY());
		smoketrails[i]->setShow(false);
	}
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
void keyboard(unsigned char key, int x, int y)                 
{
	//handles general key functions
	switch (key)
	{
	case VK_SPACE:
		{
			//if game is over and space is pressed, start a new game
			if(hud->getShots() < 1 && !bullet->getShow())
			{
				hud->reset();
				target->setShow(true);
				gameCannon->reset();
			}
			else // space will cause power level to change
			{
				powerbar->resize(5);
			}
			//if the game has newly loaded, pressing space will cause the control guide to not be drawn any more
			if(newGame)
			{
				newGame = false;
			}
		}
		break;
	case VK_ESCAPE:
		{
			//this will terminate the program
			exit(0);
		}
		break;
	case VK_RETURN:
		{
			//when enter key is pressed, if a bullet does not "exist", a new bullet will be generated,
			//using data from power bar and cannon, then set power level to zero
			if (!bullet->getShow() && hud->getShots()>0) //this ensures that the game is in progress
			{
				if(powerbar->getPower() > 0) //this prevents a shot happening if power has not been adjusted
				{
					bullet = new Bullet(gameCannon->getBulletOrigin(),16,24,powerbar->getPower(),gameCannon->getAngle());
					hud->setShots(-1);
					explosion = new Explosion(gameCannon->getBulletOrigin());
				}
			}
			powerbar->reset();	
		}
		break;
	default:
		break;
	}
}
void special_keys(int value, int x, int y)
{
	//handles special key functions such as cursors. Up arrow will rotate the cannon up, and down will rotate the cannon down
	switch (value)
	{
	case GLUT_KEY_UP:
		{
			if(newGame)
			{
				newGame= false;
			}
			gameCannon->cannonRotate(1);
		}
		break;
	case GLUT_KEY_DOWN:
		{
			if(newGame)
			{
				newGame = false;
			}
			gameCannon->cannonRotate(-1);
		}
		break;
	}
}
void display(void) 
{
	
	//clear screen
	glClear(GL_COLOR_BUFFER_BIT);
	background->drawAt(0,0);
	//draw walls
	for (int i = 0;i < MAXWALLS; i++)
	{
		walls[i]->draw();
	}
	hud->draw();
	//if bullet has been created, do it's stuff
	
	for (int i = 0; i < MAXSMOKE; ++i)
	{
		if (smoketrails[i]->getShow() && smoketrails[i] != NULL)
		{
			smoketrails[i]->draw();
		}
	}
	if (bullet->getShow())
	{
		bullet->move();
		bullet->drawBullet();
		bullet->doAcceleration();
	}
	//draw remaining game objects, then clear buffer
	gameCannon->drawCannon();
	powerbar->drawPowerBar();
	target->getShow();
	target->draw();
	//if an explosion needs to be drawn
	if (explosion != NULL)
	{
		explosion->draw();
	}
	//if the game has just loaded
	if(newGame)
	{
		controls->drawAt(144,300);
	}
	//if game is over
	if(!bullet->getShow() && hud->getShots()<1)
	{
		gameover->drawAt(272,300);
	}
	glutSwapBuffers();
}
int collision()
{
	bool collided = false;
	//this code block will handle bullet>wall collisions
	for (int i = 0; i < MAXWALLS; i++)
	{	
		float height = walls[i]->getHeight();
		float width = walls[i]->getWidth();
		float distanceX = (bullet->getLocationX()+(bullet->getXVel())/2)-walls[i]->getLocationX();
		float distanceY = (bullet->getLocationY()+(bullet->getYVel())/2)-walls[i]->getLocationY();

		if(!collided)
		{	
			//if the distance between wall X is less than width/2 and greater than -width/2 horizontal hit is true
			if(distanceX <= width/2 && distanceX >= -width/2)
			{	//if distance between wall Y is less than height/2 and greater than -height/2 vertical hit is true
				if(distanceY <= height/2 && distanceY >= -height/2)
				{
					return 1;
					collided = true;
				}
			}
		}
	}

	// this code block will handle bullet>target collision
	if (!collided)
	{
		if(target->getShow())
		{
			float distanceX = (bullet->getLocationX()+bullet->getXVel())-target->getX();
			float distanceY = (bullet->getLocationY()+bullet->getYVel())-target->getY();
			float distance = sqrt((distanceX*distanceX)+(distanceY*distanceY));

			if (target != NULL)
			{	//if difference in distance between target and bullet+speed is less than radius
				if (distance <= target->getRadius())
				{					
					collided = true;
					return 2;
				}
			}
		}
	}
	if (!collided)
	{
		return 0;
	}
	
	
}
void timer3(int animating)
{
	if (bullet->getShow())
	{
		if (numsmoketrails < 5)
		{
			smoketrails[numsmoketrails] = new SmokeTrail(bullet->getLocationX(), bullet->getLocationY());
			++numsmoketrails;
		}
		else 
		{
			numsmoketrails=0;
			smoketrails[numsmoketrails] = new SmokeTrail(bullet->getLocationX(), bullet->getLocationY());
		}
	}
}
void timer2(int animating)
{
	if (!target->getShow())
	{
		target->setShow(true);
	}
}
void collisionManager()
{
	//if a bullet "exists"
	if (bullet->getShow())
	{
		//find out if the bullet is hitting anything
		int indicator = collision();
		// if a collision has occurred
		if (indicator != 0)
		{
			//if there has been a wall/bullet collision
			if (indicator == 1)
			{
				// wall collision
				// generate an explosion at the bullet location, then hide the bullet
				explosion = new Explosion(bullet->getLocationX(),bullet->getLocationY());
				bullet->setShow(false);
				
			}
			//if there has been a target/bullet collision
			if (indicator == 2)
			{
				// target collision
				// generate an explosion at the target location, hide the bullet and target, 
				// then start a timer to respawn the target
				explosion = new Explosion(bullet->getLocationX(),bullet->getLocationY());
				bullet->setShow(false);
				target->setShow(false);
				//if there are shots remaining, respawn the target after 2 seconds
				if (hud->getShots() > 0)
				{
					glutTimerFunc(2000,timer2,0);
				}
			}
		}	
		else
		{	//this timer will create the smoke trails off the bullet
			glutTimerFunc(100,timer3,0);
		}
	}
}
void timer(int animating)
{
	
	// timer function to be called every 50 milliseconds (50fps Max)
	display();
	collisionManager();
	glutTimerFunc(20, timer, animating);
}
int main(int argc,char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("Bombardment");
	
	glClearColor(0.0, 0.0, 0.0, 1.0);
	gluOrtho2D(0, 800, 0, 600);
	init();
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special_keys);
	glutDisplayFunc(display);
	glutTimerFunc(0, timer, 0);
	glutMainLoop();
	return 0;
}
