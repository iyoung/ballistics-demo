#include "Bullet.h"


Bullet::Bullet(Vertex origin, float iHeight, float iWidth, float power, float iAngle)
{
	points[0] = new Vertex(origin.X(), origin.Y());
	points[1] = new Vertex(origin.X(), origin.Y()+iHeight);
	points[2] = new Vertex(origin.X()+iWidth, origin.Y()+iHeight/2);
	show = true;
	gravity = -0.3;
	dY= power/5*sin(iAngle);
	dX= power/5*cos(iAngle);
	bullet = new Bitmap("bullet.bmp",true);
}
float Bullet::getLocationX()
{
	//return the average of all x co-ordinates
	float fulcrum = 0;
	for (int i = 0; i < 3;i++)
	{
		fulcrum+=points[i]->X();
	}
	return fulcrum/=3;
}
float Bullet::getLocationY()
{
	//return the average of all y co-ordinates
	float fulcrum = 0;
	for (int i = 0; i < 3;i++)
	{
		fulcrum+=points[i]->Y();
	}
	return fulcrum/=3;
}
void Bullet::move()
{	
	//move the bullet to a new location based on its x and y velocities
	for (int i = 0;i<3;i++)
	{
		points[i]->addX(dX);
		points[i]->addY(dY);
	}
}
void Bullet::doAcceleration()
{
	// modify y velocity by gravity and ensure it never exceeds -11 if show is true
	if (show)
	{
		float terminalV = -12;
		if (dY > terminalV)
		{
			dY+=gravity;
		}
		else
		{
			dY = terminalV;
		}
	}
	//if not then give it zero velocity
	else
	{
		dY = 0;
		dX = 0;
	}
}
float Bullet::getXVel()
{
	//returns current x velocity
	return dX;
}
float Bullet::getYVel()
{
	//returns current y velocity
	return dY;
}
void Bullet::drawBullet()
{
	//if show is true, rotate the bullet by ang, then draw it.
	if (show)
	{	float fulcrumX = 0;
		float fulcrumY = 0;
		float fulcrum = 0;
		for (int i = 0; i < 3;i++)
		{
			fulcrumX+=points[i]->X();
			fulcrumY+=points[i]->Y();
		}
		fulcrumX/=3;
		fulcrumY/=3;
		float ang = 180*atan2(dY, dX)/3.14159268;
		glPushMatrix();
		glTranslatef(fulcrumX, fulcrumY, 0);
		glRotatef(ang, 0, 0, 1);
		glTranslatef(-fulcrumX, -fulcrumY, 0);
		bullet->drawAt(points[0]->X(),points[0]->Y());
		glPopMatrix();
	}
}
void Bullet::setShow(bool indicator)
{
	// used to set wether the bullet should be drawn
	show = indicator;
}
bool Bullet::getShow()
{
	// returns the current status of this object
	return show;
}
Bullet::~Bullet(void)
{
}
